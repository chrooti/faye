# Faye

## Why the name?

It comes from Faye Valentine, from Cowboy Bebop (and it's a short name).

## FAQ

### But it says operation not permitted

io_uring needs CAP_SYS_NICE to use the SQPOLL flag and CAP_IPC_LOCK to raise RLIMIT_MEMLOCK
You can do it with the following command:

... now needs CAP_SYS_ADMIN too

```setcap cap_sys_nice,cap_ipc_lock=pe faye```

### How to use this as a third party library?

```#include "src/server/executor.h"```

### Source directory structure

- **src/**
    - **lib/** incomplete libc reimplementation
    - **http/** http utilities (TODO)
    - **server/** simple server loop and asynchronous syscall wrappers
    - **tests/** tests (TODO)
    
## IMPORTANT:
main.cpp and http stack are WIP
