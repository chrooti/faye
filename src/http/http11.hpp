#pragma once

#include "../lib/io.h"

namespace faye::http11 {

struct Header {
    utter::BufferView name;
    utter::BufferView value;
};

struct Request {
    utter::BufferView method;
    utter::BufferView path;
    Header headers[128];
    size_t header_size;
    char* body;
};

struct Response {
    Header headers[128];
    size_t header_size;
    utter::BufferView body;
};

//GET /favicon.ico HTTP/1.1
//Host: localhost:8180
//Connection: keep-alive
//sec-ch-ua: "Chromium";v="91", " Not;A Brand";v="99"
//DNT: 1
//sec-ch-ua-mobile: ?0
//User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36
//Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8
//Sec-Fetch-Site: same-origin
//Sec-Fetch-Mode: no-cors
//Sec-Fetch-Dest: image
//Referer: http://localhost:8180/
//Accept-Encoding: gzip, deflate, br
//Accept-Language: en-US,en;q=0.9
//If-Modified-Since: Wed, 22 Jul 2009 19:15:56 GMT

int parse_request(Request* res, char* buf, size_t len) {
    auto out = utter::output();

    char* c = buf;

    char *start, *end;

    start = c;
    while (*c != ' ') {
        c++;
    }
    end = c;
    c++;

    res->method.data = start;
    res->method.len = end - start;

    start = c;
    while (*c != ' ') {
        c++;
    }
    end = c;
    c++;

    res->path.data = start;
    res->path.len = end - start;

    const char http11[] = "HTTP/1.1\r\n";
    for (size_t i = 0; i < sizeof(http11) - 1; i++) {
        if (*c != http11[i]) {
            return -1;
        }
        c++;
    }

    size_t headers_idx = 0;
    while (true) {
        if (*c == '\r') {
            c++;
            if (*c == '\n') {
                c++;
                break;
            }
            return -1;
        }

        start = c;
        while (*c != ':') {
            c++;
        }
        end = c;
        c++;

        res->headers[headers_idx].name.data = start;
        res->headers[headers_idx].name.len = end - start;

        if (*c != ' ') {
            return -1;
        }
        c++;

        start = c;
        while (*c != '\r') {
            c++;
        }
        end = c;
        c++;

        res->headers[headers_idx].value.data = start;
        res->headers[headers_idx].value.len = end - start;

        if (*c != '\n') {
            return -1;
        }
        c++;

        headers_idx++;
    }

    res->header_size = headers_idx;

    res->body = c;

    return 0;
}

size_t build_response(Response* res, char* buf, size_t len) {
    const char head[] = "HTTP/1.1 200 OK\r\n";

    char* c = buf;

    for (size_t i = 0; i < sizeof(head) - 1; i++) {
        *c = head[i];
        c++;
    }

    for (size_t i = 0; i < res->header_size; i++) {
        for (size_t j = 0; j < res->headers[i].name.len; j++) {
            *c = res->headers[i].name.data[j];
            c++;
        }

        *c = ':';
        c++;

        *c = ' ';
        c++;

        for (size_t j = 0; j < res->headers[i].value.len; j++) {
            *c = res->headers[i].value.data[j];
            c++;
        }

        *c = '\r';
        c++;

        *c = '\n';
        c++;
    }

    *c = '\r';
    c++;

    *c = '\n';
    c++;

    for (size_t i = 0; i < res->body.len; i++) {
        *c = res->body.data[i];
        c++;
    }

    return c - buf;
}

} // namespace faye::http11