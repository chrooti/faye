#pragma once

namespace faye::http2 {

/* Request */

struct Request {

};

namespace request {

void read(Request* res) {

}

void write(Request* res) {

}

} // namespace request

/* Response */

struct Response {

};

namespace response {

void read(Response* res) {

}

void write(Response* res) {

}

} // namespace response

} // namespace faye::http2