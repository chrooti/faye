#pragma once

#include <stdint.h>

#include "../lib/utils.h"

namespace faye::ssl {

#define ENUM enum __attribute__ ((__packed__))
#define STRUCT struct __attribute__((__packed__))

#if __BYTE_ORDER == __BIG_ENDIAN

// and vice versa

FAYE_INLINE constexpr uint8_t net2host(uint8_t num) {
    return num;
}

FAYE_INLINE constexpr uint16_t net2host(uint16_t num) {
    return __builtin_bswap16(num);
}

#else

FAYE_INLINE uint16_t net2host(uint16_t num) {
    return num;
}

#endif

/* Utilities */

#define SKIP(n, buf, buf_end) { \
    (buf) += (n);                          \
    if((buf) > (buf_end)) return nullptr; \
}

#define PARSE_SMALL_FIXED_FIELD(val_ptr, buf, buf_end) { \
    const uint8_t* c = buf; \
    SKIP(sizeof(*(val_ptr)), buf, buf_end);              \
    *(val_ptr) = net2host(*reinterpret_cast<const typeof(*(val_ptr))*>(c)); \
}

#define PARSE_BIG_FIXED_FIELD(val_ptr_ptr, buf, buf_end) { \
    const uint8_t* c = buf; \
    SKIP(sizeof(**(val_ptr_ptr)), buf, buf_end);              \
    *(val_ptr_ptr) = *reinterpret_cast<const typeof(*(val_ptr_ptr))*>(c); \
}

#define PARSE_VARIABLE_FIELD(size, val_ptr_ptr, buf, buf_end) { \
    const uint8_t* c = buf; \
    SKIP((size) * sizeof(**(val_ptr_ptr)), buf, buf_end);              \
    *(val_ptr_ptr) = *reinterpret_cast<const typeof(*(val_ptr_ptr))*>(c); \
}

//template<typename T>
//FAYE_INLINE const uint8_t* parse_variable_field(size_t size, T** val, const uint8_t* buf, const uint8_t* buf_end) {
//    const uint8_t* c = buf;
//    FAYE_SSL_SKIP(size, buf, buf_end);
//
//    *val = *reinterpret_cast<T* const*>(c);
//
//    return buf;
//}

using ProtocolVersion = uint16_t;

/* Supported Versions Extension */

// client_hello
STRUCT ClientSupportedVersionsExt {
    ProtocolVersion selected_version;
};

const uint8_t* parse_client_supported_versions_ext(const uint8_t* buf, const uint8_t* buf_end) {
    ProtocolVersion selected_version;
    PARSE_SMALL_FIXED_FIELD(&selected_version, buf, buf_end)

    if(selected_version != 0x0304) {
        return nullptr;
    }

    return buf;
}

// server_hello and hello_retry_request
template<uint8_t size>
STRUCT ServerSupportedVersionExt {
    // length between 2 and 254
    ProtocolVersion versions[size];
};

const uint8_t* parse_server_supported_versions_ext(const uint8_t* buf, const uint8_t* buf_end) {
    uint8_t versions_len;
    PARSE_SMALL_FIXED_FIELD(&versions_len, buf, buf_end)
    if(versions_len <= 2 || versions_len == 255) {
        return nullptr;
    }

    ProtocolVersion* versions;
    PARSE_VARIABLE_FIELD(versions_len, &versions, buf, buf_end)
    for(uint8_t i = 0; i < versions_len; i++) {
        if(versions[i] == net2host(static_cast<uint16_t>(0x0304))) {
            return buf;
        }
    }

    return nullptr;
}

/* Cookie Extension */

template<uint16_t size>
STRUCT CookieExt {
    // length between 1 and 2^16-1
    uint8_t cookie[size];
};

const uint8_t* parse_cookie_ext(const uint8_t* buf, const uint8_t* buf_end) {
    uint16_t cookie_len;
    PARSE_SMALL_FIXED_FIELD(&cookie_len, buf, buf_end)
    if(cookie_len == 0) {
        return nullptr;
    }

    // TODO: save this somewhere
    uint8_t* cookie;
    PARSE_VARIABLE_FIELD(cookie_len, &cookie, buf, buf_end)
    return buf;
}

/* Signature Algorithms Extension */

ENUM SignatureScheme {
    // RSASSA-PKCS1-v1_5
    RSA_PKCS1_SHA256 = 0x0401,
    RSA_PKCS1_SHA384 = 0x0501,
    RSA_PKCS1_SHA512 = 0x0601,

    // ECDSA
    ECDSA_SECP256R1_SHA256 = 0x0403,
    ECDSA_SECP384R1_SHA384 = 0x0503,
    ECDSA_SECP521R1_SHA512 = 0x0603,

    // RSASSA-PSS signature with rsaEncryption public key
    RSA_PSS_RSAE_SHA256 = 0x0804,
    RSA_PSS_RSAE_SHA384 = 0x0805,
    RSA_PSS_RSAE_SHA512 = 0x0806,

    // EdDSA
    ED25519 = 0x0807,
    ED448 = 0x0808,

    // RSASSA-PSS signature with RSASSA-PSS public key
    RSA_PSS_PSS_SHA256 = 0x0809,
    RSA_PSS_PSS_SHA384 = 0x080a,
    RSA_PSS_PSS_SHA512 = 0x080b,

    // Legacy
    RSA_PKCS1_SHA1 = 0x0201,
    ECDSA_SHA1 = 0x0203,

    SIGNATURE_SCHEME_MAX = 0xffff
};

template<uint16_t size>
STRUCT SignatureAlgorithmsExt {
    // length between 2 and 2^16-2
    // SignatureSchemeList
    SignatureScheme supported_signature_algorithms[size];
};

const uint8_t* parse_signature_algorithms_ext(const uint8_t* buf, const uint8_t* buf_end) {
    uint16_t supported_signature_algorithms_len;
    PARSE_SMALL_FIXED_FIELD(&supported_signature_algorithms_len, buf, buf_end)
    if(supported_signature_algorithms_len <= 2 || supported_signature_algorithms_len == UINT16_MAX) {
        return nullptr;
    }

    SignatureScheme* supported_signature_algorithms;
    PARSE_VARIABLE_FIELD(supported_signature_algorithms_len, &supported_signature_algorithms, buf, buf_end)
    for(uint16_t i = 0; i < supported_signature_algorithms_len; i++) {
        // TODO: implement one of those
        return buf;
    }

    return nullptr;
}

/* Supported Groups Extension */

ENUM NamedGroup {
    // ECDHE
    SECP256R1 = 0x0017,
    SECP384R1 = 0x0018,
    SECP521R1 = 0x0019,
    X25519 = 0x001d,
    X448 = 0x001e,

    // DHE
    FFDHE2048 = 0x0100,
    FFDHE3072 = 0x0101,
    FFDHE4096 = 0x0102,
    FFDHE6144 = 0x0103,
    FFDHE8192 = 0x0104,

    NAMED_GROUP_MAX = 0xffff
};

template<uint16_t size>
STRUCT SupportedGroupsExt {
    // length between 2 and 2^16-1
    // NamedGroupList
    NamedGroup named_group_list[size];
};

const uint8_t* parse_supported_groups_ext(const uint8_t* buf, const uint8_t* buf_end) {
    uint16_t named_group_list_len;
    PARSE_SMALL_FIXED_FIELD(&named_group_list_len, buf, buf_end)
    if(named_group_list_len <= 2) {
        return nullptr;
    }

    NamedGroup* named_group_list;
    PARSE_VARIABLE_FIELD(named_group_list_len, &named_group_list, buf, buf_end)
    for(uint16_t i = 0; i < named_group_list_len; i++) {
        // TODO: implement one of those
        return buf;
    }
    return buf;
}

/* Key Share Extension */

template<uint16_t size>
struct KeyShareEntry {
    NamedGroup group;
    uint8_t key_exchange[size];
};

struct KeyShareEntryPtr {
    NamedGroup group;
    uint16_t key_exchange_size;
    uint8_t* key_exchange;
};

struct KeyShareClientHelloExt {
    KeyShareEntryPtr* client_shares;
};

struct KeyShareHelloRetryRequestExt {
    KeyShareEntryPtr* client_shares;
};

struct KeyShareServerHello {
    KeyShareEntryPtr server_share;
};

const uint8_t* parse_key_share_client_hello_ext(const uint8_t* buf, const uint8_t* buf_end) {
    // length between 0 and 2^16-1
    uint16_t client_shares_len;
    PARSE_SMALL_FIXED_FIELD(&client_shares_len, buf, buf_end)

    for(uint16_t i = 0; i < client_shares_len; i++) {
        NamedGroup group;
        PARSE_SMALL_FIXED_FIELD(reinterpret_cast<uint16_t*>(&group), buf, buf_end)

        // between 0 and 2^16-1
        uint16_t key_exchange_len;
        PARSE_SMALL_FIXED_FIELD(&key_exchange_len, buf, buf_end)
        if(key_exchange_len == 0) {
            return nullptr;
        }
    }
    PARSE_VARIABLE_FIELD(ext->named_group_list_len, &ext->named_group_list, buf, buf_end)
    return buf;
}

/* Extensions */

ENUM ExtensionType {
    SERVER_NAME = 0,
    MAX_FRAGMENT_LENGTH = 1,
    STATUS_REQUEST = 5,
    SUPPORTED_GROUPS = 10,
    SIGNATURE_ALGORITHMS = 13,
    USE_SRTP = 14,
    HEARTBEAT = 15,
    APPLICATION_LAYER_PROTOCOL_NEGOTIATION = 16,
    SIGNED_CERTIFICATE_TIMESTAMP = 18,
    CLIENT_CERTIFICATE_TYPE = 19,
    SERVER_CERTIFICATE_TYPE = 20,
    PADDING = 21,
    PRE_SHARED_KEY = 41,
    EARLY_DATA = 42,
    SUPPORTED_VERSIONS = 43,
    COOKIE = 44,
    PSK_KEY_EXCHANGE_MODES = 45,
    CERTIFICATE_AUTHORITIES = 47,
    OLD_FILTERS = 48,
    POST_HANDSHAKE_AUTH = 48,
    SIGNATURE_ALGORITHMS_CERT = 50,
    KEY_SHARE = 51,
    EXTENSION_TYPE_MAX = 65535
};

template<
    uint16_t supported_groups_size,
    uint16_t signature_algorithms_size,
    uint8_t server_supported_version_size,
    uint16_t cookie_size,
    uint16_t signature_algorithms_cert_size
>
struct Extensions {
    SupportedGroupsExt<supported_groups_size> supported_groups;
    SignatureAlgorithmsExt<signature_algorithms_size> signature_algorithms;
    ClientSupportedVersionsExt client_supported_version;
    ServerSupportedVersionExt<server_supported_version_size> server_supported_version;
    CookieExt<cookie_size> cookie;
    SignatureAlgorithmsExt<signature_algorithms_cert_size> signature_algorithms_cert;
};

template<bool is_client>
const uint8_t* parse_extensions(const uint8_t* buf, const uint8_t* buf_end) {
    uint16_t ext_len;
    PARSE_SMALL_FIXED_FIELD(&ext_len, buf, buf_end)

    for(uint16_t i = 0; i < ext_len && buf != nullptr; i++) {
        uint16_t ext_type;
        PARSE_SMALL_FIXED_FIELD(&ext_type, buf, buf_end)
        switch(ext_type) {
        case SUPPORTED_GROUPS:
            buf = parse_supported_groups_ext(buf, buf_end);
            break;
        case SIGNATURE_ALGORITHMS:
            buf = parse_signature_algorithms_ext(buf, buf_end);
            break;
        case SUPPORTED_VERSIONS:
            if(is_client) {
                buf = parse_client_supported_versions_ext(buf, buf_end);
            } else {
                buf = parse_server_supported_versions_ext(buf, buf_end);
            }
            break;
        case COOKIE:
            buf = parse_cookie_ext(buf, buf_end);
            break;
        case SIGNATURE_ALGORITHMS_CERT:
            buf = parse_signature_algorithms_ext(buf, buf_end);
            break;
        }
    }
    return buf;
}

/* ClientHello */

using CipherSuite = uint8_t[2];

struct ClientHello {
    uint8_t (*random)[32];

    // length between 2 and 2^16 -2
    uint16_t cipher_suites_len;
    CipherSuite* cipher_suites;

    Extensions extensions;
};

const uint8_t* parse_client_hello(const uint8_t* buf, const uint8_t* buf_end) {
    ProtocolVersion legacy_version;

    PARSE_SMALL_FIXED_FIELD(&legacy_version, buf, buf_end)

    if(legacy_version != 0x0303) {
        return nullptr;
    }

    // random
    uint8_t (*random)[32];
    PARSE_BIG_FIXED_FIELD(&random, buf, buf_end);

    // legacy_session_id
    // length between 0 and 32 -> 1 byte
    uint8_t legacy_session_id_length;
    PARSE_SMALL_FIXED_FIELD(&legacy_session_id_length, buf, buf_end)

    // skip it, we are TLS v1.3 *only*
    buf += legacy_session_id_length;

    // cipher_suites
    uint16_t cipher_suites_len;
    PARSE_SMALL_FIXED_FIELD(&cipher_suites_len, buf, buf_end)
    if(cipher_suites_len <= 2) {
        return nullptr;
    }

    CipherSuite* cipher_suites;
    PARSE_VARIABLE_FIELD(cipher_suites_len, &cipher_suites, buf, buf_end)

    // legacy_compression_method
    // length between 1 and 2^8 - 1
    uint8_t legacy_compression_method_len;
    PARSE_SMALL_FIXED_FIELD(&legacy_compression_method_len, buf, buf_end)
    if(legacy_compression_method_len != 0) {
        return nullptr;
    }

    return parse_extensions<true>(buf, buf_end);
}

/* ServerHello */

template<typename ...Args>
struct ServerHello {
//    ProtocolVersion legacy_version; /* = 0x0303 */
//    Random (*random)[32]; // there are special values
//    char* legacy_session_id; // size between 0 and 32
//    CipherSuite* cipher_suite;
//    uint8_t legacy_compression_method; /* = 0 */
//    uint16_t extensions_len;
//    Extensions<Args...> extensions;
};

/* Handshake */

ENUM HandshakeType {
    CLIENT_HELLO = 1,
    SERVER_HELLO = 2,
    NEW_SESSION_TICKET = 4,
    END_OF_EARLY_DATA = 5,
    ENCRYPTED_EXTENSIONS = 8,
    CERTIFICATE = 11,
    CERTIFICATE_REQUEST = 13,
    CERTIFICATE_VERIFY = 15,
    FINISHED = 20,
    KEY_UPDATE = 24,
    MESSAGE_HASH = 254,
    HANDSHAKE_TYPE_MAX = 255,
};

struct EndOfEarlyData {};
struct EncryptedExtensions {};
struct CertificateRequest {};
struct Certificate {};
struct CertificateVerify {};
struct Finished {};
struct NewSessionTicket {};
struct KeyUpdate {};

struct Handshake {
    HandshakeType msg_type;
    uint32_t length; // 24 bit max
    union {
        ClientHello client_hello;
        ServerHello server_hello;
        EndOfEarlyData end_of_early_data;
        EncryptedExtensions encrypted_extensions;
        CertificateRequest certificate_request;
        Certificate certificate;
        CertificateVerify certificate_verify;
        Finished finished;
        NewSessionTicket new_session_ticket;
        KeyUpdate key_update;
    };
};

const uint8_t* parse_handshake(const uint8_t* buf, const uint8_t* buf_end) {
    HandshakeType msg_type;
    PARSE_SMALL_FIXED_FIELD(reinterpret_cast<uint8_t*>(&msg_type), buf, buf_end)

    // length
    auto len_bytes = *reinterpret_cast<const uint8_t(*)[3]>(buf);
    uint32_t len = (len_bytes[0] << 16) | (len_bytes[1] << 8) | (len_bytes[2]);

    buf += 3;

    switch(msg_type) {
    case CLIENT_HELLO:
        return parse_client_hello(buf, buf_end);
    }

    return nullptr;
}

/* TLSPlaintext */

ENUM ContentType  {
    INVALID = 0,
    CHANGE_CIPHER_SPEC = 20,
    ALERT = 21,
    HANDSHAKE = 22,
    APPLICATION_DATA = 23,
    CONTENT_TYPE_MAX = 255
};

STRUCT TLSPlainText {
    ContentType type;
    uint16_t len;
    Handshake handshake;
};

const uint8_t* parse_tlsplaintext(const uint8_t* buf, const uint8_t* buf_end) {
    ContentType content_type;
    PARSE_SMALL_FIXED_FIELD(reinterpret_cast<uint8_t*>(&content_type), buf, buf_end)

    // skip legacy_record_version
    SKIP(2, buf, buf_end)

    // length
    uint16_t length;
    PARSE_SMALL_FIXED_FIELD(&length, buf, buf_end)

    switch(content_type) {
    case HANDSHAKE:
        return parse_handshake(buf, buf_end);
    }

    return nullptr;
}

bool parse_tls(TLSPlainText* tls_plain_text, const uint8_t* buf, const uint8_t* buf_end) {
    buf = parse_tlsplaintext(buf, buf_end);

    return buf == buf_end;
}

} // namespace faye::ssl
