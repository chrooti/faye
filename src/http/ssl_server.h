#pragma once

#include <stdint.h>

#include "../lib/io.h"
#include "../lib/utils.h"

#define STRUCT struct __attribute__((__packed__))

// NOTE: this must be included by the server

namespace faye::ssl {

utter::Output<256> global_out = utter::output<256>();
#define DEBUG(...) utter::to(global_out, __VA_ARGS__, "\n", utter::to_stdout)

struct Context {
    uint16_t cookie_len;
    const uint8_t* cookie;
};

#if __BYTE_ORDER == __BIG_ENDIAN

// and vice versa

FAYE_INLINE uint8_t net2host(uint8_t num) {
    return num;
}

FAYE_INLINE uint16_t net2host(uint16_t num) {
    return __builtin_bswap16(num);
}

FAYE_INLINE uint32_t net2host(uint32_t num) {
    return __builtin_bswap32(num);
}

#else

FAYE_INLINE uint8_t net2host(uint8_t num) {
    return num;
}

FAYE_INLINE uint16_t net2host(uint16_t num) {
    return num;
}

FAYE_INLINE uint32_t net2host(uint32_t num) {
    return num;
}

#endif

/* Utilities */

#define PARSE_FIELD(size, val_ptr, buf, buf_end) \
    (val_ptr) = reinterpret_cast<const typeof(*(val_ptr))*>(buf); \
    (buf) += (size);

#define PARSE_SMALL_FIELD(val, buf, buf_end) \
    (val) = net2host(*reinterpret_cast<const typeof(val)*>(buf)); \
    (buf) += sizeof(val);

#define PARSE_LEN_FIELD(val_ptr, buf, buf_end) { \
    PARSE_SMALL_FIELD(val_ptr, buf, buf_end) \
    if((buf) + (val_ptr) > (buf_end)) return nullptr;                                                \
}

// buf_end here is field_end if it's an array inside of an array
#define PARSE_ARRAY(field_name, buf, buf_end) \
    const uint8_t* field_name ## _end = (buf) + field_name ## _len; \
    if(field_name ## _end > (buf_end)) return nullptr;                        \
    while((buf) != field_name ## _end)

using ProtocolVersion = uint16_t;
using CipherSuite = uint16_t;

/* Supported Version Extension */

const uint8_t* parse_supported_versions(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    // length between 2 and 254
    uint8_t versions_len;
    PARSE_LEN_FIELD(versions_len, buf, buf_end)
    if(versions_len < 2 || versions_len == 255) {
        return nullptr;
    }

    PARSE_ARRAY(versions, buf, buf_end) {
        ProtocolVersion version;
        PARSE_SMALL_FIELD(version, buf, buf_end)

        // TLS v1.3
        if(version == 0x0304) {
            return versions_end;
        }
    }

    return nullptr;
}

/* Cookie Extension */

const uint8_t* parse_cookie(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    // length between 1 and 2^16-1
    uint16_t cookie_len;
    PARSE_LEN_FIELD(cookie_len, buf, buf_end)
    if(cookie_len == 0) {
        return nullptr;
    }

    const uint8_t* cookie;
    PARSE_FIELD(cookie_len, cookie, buf, buf_end)

    ctx->cookie_len = cookie_len;
    ctx->cookie = cookie;
    
    return buf;
}

/* Signature Algorithms Extension */

enum {
    // RSASSA-PKCS1-v1_5
    SIGNATURE_SCHEME_RSA_PKCS1_SHA256 = 0x0401,
    SIGNATURE_SCHEME_RSA_PKCS1_SHA384 = 0x0501,
    SIGNATURE_SCHEME_RSA_PKCS1_SHA512 = 0x0601,

    // ECDSA
    SIGNATURE_SCHEME_ECDSA_SECP256R1_SHA256 = 0x0403,
    SIGNATURE_SCHEME_ECDSA_SECP384R1_SHA384 = 0x0503,
    SIGNATURE_SCHEME_ECDSA_SECP521R1_SHA512 = 0x0603,

    // RSASSA-PSS signature with rsaEncryption public key
    SIGNATURE_SCHEME_RSA_PSS_RSAE_SHA256 = 0x0804,
    SIGNATURE_SCHEME_RSA_PSS_RSAE_SHA384 = 0x0805,
    SIGNATURE_SCHEME_RSA_PSS_RSAE_SHA512 = 0x0806,

    // EdDSA
    SIGNATURE_SCHEME_ED25519 = 0x0807,
    SIGNATURE_SCHEME_ED448 = 0x0808,

    // RSASSA-PSS signature with RSASSA-PSS public key
    SIGNATURE_SCHEME_RSA_PSS_PSS_SHA256 = 0x0809,
    SIGNATURE_SCHEME_RSA_PSS_PSS_SHA384 = 0x080a,
    SIGNATURE_SCHEME_RSA_PSS_PSS_SHA512 = 0x080b,

    // Legacy
    SIGNATURE_SCHEME_RSA_PKCS1_SHA1 = 0x0201,
    SIGNATURE_SCHEME_ECDSA_SHA1 = 0x0203,
};

using SignatureScheme = uint16_t;

const uint8_t* parse_signature_algorithms(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    // length between 2 and 2^16 - 2
    uint16_t supported_signature_algorithms_len;
    PARSE_LEN_FIELD(supported_signature_algorithms_len, buf, buf_end)
    if(supported_signature_algorithms_len < 2 || supported_signature_algorithms_len == UINT16_MAX) {
        return nullptr;
    }

    PARSE_ARRAY(supported_signature_algorithms, buf, buf_end) {
        SignatureScheme scheme;
        PARSE_SMALL_FIELD(scheme, buf, buf_end)

        DEBUG("sigalg: ", utter::int2hex(scheme));
    }

    return buf;
}

/* Supported Groups Extension */

enum {
    // ECDHE
    NAMED_GROUP_SECP256R1 = 0x0017,
    NAMED_GROUP_SECP384R1 = 0x0018,
    NAMED_GROUP_SECP521R1 = 0x0019,
    NAMED_GROUP_X25519 = 0x001d,
    NAMED_GROUP_X448 = 0x001e,

    // FFDHE
    NAMED_GROUP_FFDHE2048 = 0x0100,
    NAMED_GROUP_FFDHE3072 = 0x0101,
    NAMED_GROUP_FFDHE4096 = 0x0102,
    NAMED_GROUP_FFDHE6144 = 0x0103,
    NAMED_GROUP_FFDHE8192 = 0x0104,
};

using NamedGroup = uint16_t;

const uint8_t* parse_supported_groups(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    // length between 2 and 2^16 - 2
    uint16_t named_group_list_len;
    PARSE_LEN_FIELD(named_group_list_len, buf, buf_end)
    if(named_group_list_len < 2 || named_group_list_len == UINT16_MAX) {
        return nullptr;
    }

    PARSE_ARRAY(named_group_list, buf, buf_end) {
        NamedGroup named_group;
        PARSE_SMALL_FIELD(named_group, buf, buf_end)

        // TODO; DO something with named_groups
    }
    return buf;
}

/* Key Share Extension */

const uint8_t* parse_key_share(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    // length between 0 and 2^16 - 1
    uint16_t client_shares_len;
    PARSE_SMALL_FIELD(client_shares_len, buf, buf_end)

    PARSE_ARRAY(client_shares, buf, buf_end) {
        NamedGroup group;
        PARSE_SMALL_FIELD(group, buf, buf_end)

        // between 0 and 2^16-1
        uint16_t key_exchange_len;
        PARSE_LEN_FIELD(key_exchange_len, buf, client_shares_end)
        if(key_exchange_len == 0) {
            return nullptr;
        }

        switch(group) {
        // ECDHE
        case NAMED_GROUP_SECP256R1:
        case NAMED_GROUP_SECP384R1:
        case NAMED_GROUP_SECP521R1:
        case NAMED_GROUP_X25519:
        case NAMED_GROUP_X448:
            break;

        // FFDHE
        case NAMED_GROUP_FFDHE2048:
        case NAMED_GROUP_FFDHE3072:
        case NAMED_GROUP_FFDHE4096:
        case NAMED_GROUP_FFDHE6144:
        case NAMED_GROUP_FFDHE8192:
            break;
        }

        // TODO implement this
        uint8_t* key_exchange;

        buf += key_exchange_len;
    }

    return buf;
}

/* Server Name Indication Extension (RFC 6066 section 3) */

const uint8_t* parse_server_name(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    // length between 0 and 2^16 - 1
    uint16_t server_name_list_len;
    PARSE_LEN_FIELD(server_name_list_len, buf, buf_end)
    if(server_name_list_len == 0) {
        return nullptr;
    }

    PARSE_ARRAY(server_name_list, buf, buf_end) {
        uint8_t name_type;
        PARSE_SMALL_FIELD(name_type, buf, buf_end)
        if(name_type != 0) {
            return nullptr;
        }

        // length between 1 and 2^16 - 1
        uint16_t host_name_len;
        PARSE_LEN_FIELD(host_name_len, buf, server_name_list_end)
        if(host_name_len == 0) {
            return nullptr;
        }

        uint8_t* host_name;
        buf += host_name_len;
        // TODO: do something with host_name
    }

    return buf;
}

/* Application-Layer Protocol Negotiation Extension (RFC 7301) */

const uint8_t* parse_alpn(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    // length between 2 and 2^16 - 1
    uint16_t protocol_name_list_len;
    PARSE_LEN_FIELD(protocol_name_list_len, buf, buf_end)
    if(protocol_name_list_len < 2) {
        return nullptr;
    }

    PARSE_ARRAY(protocol_name_list, buf, buf_end) {
        uint8_t protocol_name_len;
        PARSE_LEN_FIELD(protocol_name_len, buf, protocol_name_list_end)
        if(protocol_name_len == 0) {
            return nullptr;
        }

        uint8_t* protocol_name;
        buf += protocol_name_len;
    }

    return buf;
}

/* Extensions */

enum {
    EXTENSION_TYPE_SERVER_NAME = 0,
    EXTENSION_TYPE_MAX_FRAGMENT_LENGTH = 1,
    EXTENSION_TYPE_STATUS_REQUEST = 5,
    EXTENSION_TYPE_SUPPORTED_GROUPS = 10,
    EXTENSION_TYPE_SIGNATURE_ALGORITHMS = 13,
    EXTENSION_TYPE_USE_SRTP = 14,
    EXTENSION_TYPE_HEARTBEAT = 15,
    EXTENSION_TYPE_APPLICATION_LAYER_PROTOCOL_NEGOTIATION = 16,
    EXTENSION_TYPE_SIGNED_CERTIFICATE_TIMESTAMP = 18,
    EXTENSION_TYPE_CLIENT_CERTIFICATE_TYPE = 19,
    EXTENSION_TYPE_SERVER_CERTIFICATE_TYPE = 20,
    EXTENSION_TYPE_PADDING = 21,
    EXTENSION_TYPE_PRE_SHARED_KEY = 41,
    EXTENSION_TYPE_EARLY_DATA = 42,
    EXTENSION_TYPE_SUPPORTED_VERSIONS = 43,
    EXTENSION_TYPE_COOKIE = 44,
    EXTENSION_TYPE_PSK_KEY_EXCHANGE_MODES = 45,
    EXTENSION_TYPE_CERTIFICATE_AUTHORITIES = 47,
    EXTENSION_TYPE_OLD_FILTERS = 48,
    EXTENSION_TYPE_POST_HANDSHAKE_AUTH = 48,
    EXTENSION_TYPE_SIGNATURE_ALGORITHMS_CERT = 50,
    EXTENSION_TYPE_KEY_SHARE = 51,
};

using ExtensionType = uint16_t;

const uint8_t* parse_extensions(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    uint16_t extensions_len;
    PARSE_LEN_FIELD(extensions_len, buf, buf_end)
    if(extensions_len < 8) {
        return nullptr;
    }

    while(buf != buf_end && buf != nullptr) {
        ExtensionType ext_type;
        PARSE_SMALL_FIELD(ext_type, buf, buf_end)

        // length is between 0 and 2^16 - 1
        uint16_t ext_len;
        PARSE_LEN_FIELD(ext_len, buf, buf_end)

        DEBUG("ext_type: ", utter::int2dec(ext_type));
        DEBUG("ext_len: ", utter::int2dec(ext_len));

        switch(ext_type) {
        case EXTENSION_TYPE_SERVER_NAME:
            buf = parse_server_name(ctx, buf, buf_end);
            break;
        case EXTENSION_TYPE_SUPPORTED_GROUPS:
            buf = parse_supported_groups(ctx, buf, buf_end);
            break;
        case EXTENSION_TYPE_SIGNATURE_ALGORITHMS:
            buf = parse_signature_algorithms(ctx, buf, buf_end);
            break;
        case EXTENSION_TYPE_APPLICATION_LAYER_PROTOCOL_NEGOTIATION:
            buf = parse_alpn(ctx, buf, buf_end);
            break;
        case EXTENSION_TYPE_SUPPORTED_VERSIONS:
            buf = parse_supported_versions(ctx, buf, buf_end);
            break;
        case EXTENSION_TYPE_COOKIE:
            buf = parse_cookie(ctx, buf, buf_end);
            break;
        case EXTENSION_TYPE_SIGNATURE_ALGORITHMS_CERT:
            buf = parse_signature_algorithms(ctx, buf, buf_end);
            break;
        case EXTENSION_TYPE_KEY_SHARE:
            buf = parse_key_share(ctx, buf, buf_end);
            break;
        default:
            DEBUG("unknown extension");
            buf += ext_len;
            break;
        }
    }

    DEBUG("total_len: ", utter::int2dec(buf_end - buf));
    DEBUG("------");

    return buf;
}

/* ClientHello */

const uint8_t* parse_client_hello(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    ProtocolVersion legacy_version;
    PARSE_SMALL_FIELD(legacy_version, buf, buf_end)
    if(legacy_version != 0x0303) {
        return nullptr;
    }

    // random
    const uint8_t *random;
    PARSE_FIELD(32, random, buf, buf_end)

    // legacy_session_id
    // length between 0 and 32
    uint8_t legacy_session_id_length;
    PARSE_LEN_FIELD(legacy_session_id_length, buf, buf_end)

    // skip it, we are TLS v1.3 *only*
    buf += legacy_session_id_length;

    // cipher_suites
    // length between 2 and 2^16 - 2
    uint16_t cipher_suites_len;
    PARSE_LEN_FIELD(cipher_suites_len, buf, buf_end)
    if(cipher_suites_len < 2 || cipher_suites_len == UINT16_MAX) {
        return nullptr;
    }

    PARSE_ARRAY(cipher_suites, buf, buf_end) {
        CipherSuite cipher_suite;
        PARSE_SMALL_FIELD(cipher_suite, buf, buf_end)
    }

    // legacy_compression_methods -> must be an array with len = 1 and arr[0] = 0
    uint16_t legacy_compression_methods;
    PARSE_SMALL_FIELD(legacy_compression_methods, buf, buf_end)
    if(legacy_compression_methods != 0x0100) {
        return nullptr;
    }

    return parse_extensions(ctx, buf, buf_end);
}

/* Handshake */

enum {
    HANDSHAKE_TYPE_CLIENT_HELLO = 1,
    HANDSHAKE_TYPE_SERVER_HELLO = 2,
    HANDSHAKE_TYPE_NEW_SESSION_TICKET = 4,
    HANDSHAKE_TYPE_END_OF_EARLY_DATA = 5,
    HANDSHAKE_TYPE_ENCRYPTED_EXTENSIONS = 8,
    HANDSHAKE_TYPE_CERTIFICATE = 11,
    HANDSHAKE_TYPE_CERTIFICATE_REQUEST = 13,
    HANDSHAKE_TYPE_CERTIFICATE_VERIFY = 15,
    HANDSHAKE_TYPE_FINISHED = 20,
    HANDSHAKE_TYPE_KEY_UPDATE = 24,
    HANDSHAKE_TYPE_MESSAGE_HASH = 254,
};

using HandshakeType = uint8_t;

const uint8_t* parse_handshake(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    HandshakeType msg_type;
    PARSE_SMALL_FIELD(msg_type, buf, buf_end)

    // it's a 24 bit field, so I do this manually
    uint32_t msg_len = net2host(*reinterpret_cast<const uint32_t*>(buf));
    msg_len >>= 8;
    (buf) += sizeof(uint8_t) * 3;

    if (buf + msg_len > buf_end) {
        return nullptr;
    }

    switch(msg_type) {
    case HANDSHAKE_TYPE_CLIENT_HELLO:
        return parse_client_hello(ctx, buf, buf_end);
    }

    return nullptr;
}

/* TLSPlainText */

enum {
    CONTENT_TYPE_INVALID = 0,
    CONTENT_TYPE_CHANGE_CIPHER_SPEC = 20,
    CONTENT_TYPE_ALERT = 21,
    CONTENT_TYPE_HANDSHAKE = 22,
    CONTENT_TYPE_APPLICATION_DATA = 23,
};

using ContentType = uint8_t;

const uint8_t* parse_tls_plaintext(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    size_t header_size = sizeof(ContentType) + 2 + sizeof(uint16_t);
    if(buf + header_size >= buf_end) {
        return nullptr;
    }

    ContentType content_type;
    PARSE_SMALL_FIELD(content_type, buf, buf_end)

    // skip legacy_record_version
    buf += 2;

    // length
    uint16_t content_len;
    PARSE_LEN_FIELD(content_len, buf, buf_end)

    switch(content_type) {
    case CONTENT_TYPE_HANDSHAKE:
        return parse_handshake(ctx, buf, buf_end);
    default:
        return nullptr;
    }
}

bool read(Context* ctx, const uint8_t* buf, const uint8_t* buf_end) {
    const uint8_t* read_buf = parse_tls_plaintext(ctx, buf, buf_end);
    return read_buf == buf_end;
}

bool write(Context* ctx, const uint8_t* buf) {
    return false;
}

} // namespace faye::ssl