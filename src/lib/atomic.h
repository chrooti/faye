#pragma once

#include "utils.h"

namespace faye::atomic {

template<typename T>
FAYE_INLINE T load_acquire(const T* p) {
    return __atomic_load_n(p, __ATOMIC_ACQUIRE);
}

template<typename T>
FAYE_INLINE void store_release(T* p, T v) {
    return __atomic_store_n(p, v, __ATOMIC_RELEASE);
}

// ADD, ADC, AND, BTC, BTR, BTS, CMPXCHG, CMPXCH8B, CMPXCHG16B, DEC, INC, NEG,
// NOT, OR, SBB, SUB, XOR, XADD, and XCHG

template<typename T, typename U>
FAYE_INLINE T add(T* val, U add) {
    asm volatile("lock add %1, %0" : "+m"(*val) : "r"(add) : "memory", "cc");
    return *val;
}

template<typename T, typename U, typename V>
FAYE_INLINE bool cmpxchg(T* val, U cmp, V set) {
    // compares cmp with val, if the two are equals set is loaded in the val
    // address

    unsigned char res;
    asm volatile("lock cmpxchg %2, %1\n"
                 "sete %0"
                 : "=a"(res)
                 : "m"(*val), "r"(set), "a"(cmp)
                 : "memory");
    return res;
}

template<typename T, typename U>
FAYE_INLINE U xadd(T* val, U add) {
    asm volatile("lock xadd %0, %1" : "+r"(add) : "m"(*val) : "memory", "cc");
    return add;
}

template<typename T, typename U>
FAYE_INLINE U xchg(T* val, U set) {
    asm volatile("lock xchg %0, %1" : "+r"(set) : "m"(*val) : "memory", "cc");
    return set;
}

} // namespace faye::atomic