#pragma once

#include <linux/fcntl.h>
#include <linux/uio.h>

#include "syscall.h"

#include <utter/utter.hpp>

// needed for utter
ssize_t writev(int fd, const iovec* iov, int iovcnt) {
    return writev(static_cast<unsigned long>(fd), iov, static_cast<unsigned long>(iovcnt));
}

namespace faye::io {

long read_file(const char* path, char* buf, size_t count) {
    long fd = openat(AT_FDCWD, path, 0, O_RDONLY);
    if (fd < 0) {
        return fd;
    }

    long ret = read(fd, buf, count);
    close(fd);

    return ret;
}

} // namespace faye::io
