#pragma once

#include <stddef.h>

#include <linux/eventpoll.h>
#include <linux/io_uring.h>
#include <linux/signal.h>
#include <linux/stat.h>
#include <linux/swab.h>
#include <linux/time.h>
#include <linux/uio.h>

#include "atomic.h"
#include "memory.h"
#include "syscall.h"

namespace faye::io_uring {

/* submission queue */
struct SQ {
    unsigned* head;
    unsigned* tail;
    unsigned* ring_mask;
    unsigned* ring_entries;
    unsigned* flags;
    unsigned* dropped;
    unsigned* array;
    io_uring_sqe* sqes;
};

/* completion queue */
struct CQ {
    unsigned* head;
    unsigned* tail;
    unsigned* ring_mask;
    unsigned* ring_entries;
    unsigned* flags;
    unsigned* overflow;
    io_uring_cqe* cqes;
};

} // namespace faye::io_uring

namespace faye {

struct IoUring {
    io_uring::SQ sq;
    io_uring::CQ cq;
    size_t ring_size;
    void* ring_ptr;
    int fd;
};

} // namespace faye

namespace faye::io_uring {

/* INIT AND CLEAN */

int init(IoUring* io_uring, int cpu, unsigned idle) {
    io_uring_params params {};
    params.flags = IORING_SETUP_SQPOLL | IORING_SETUP_SQ_AFF | IORING_SETUP_CLAMP;
    params.sq_thread_cpu = cpu;
    params.sq_thread_idle = idle;

    int fd = static_cast<int>(io_uring_setup(-1, &params));
    if (fd < 0) {
        return fd;
    }
    io_uring->fd = fd;

    // submission queue setup

    io_sqring_offsets* sq_off = &params.sq_off;
    SQ* sq = &io_uring->sq;

    io_uring->ring_size = sq_off->array + params.sq_entries * sizeof(unsigned);
    io_uring->ring_ptr = memory::mmap(nullptr, io_uring->ring_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_POPULATE, fd, IORING_OFF_SQ_RING);
    if (io_uring->ring_ptr == nullptr) {
        return 0;
    }
    auto ring_ptr = reinterpret_cast<char*>(io_uring->ring_ptr);

    sq->head = reinterpret_cast<unsigned*>(ring_ptr + sq_off->head);
    sq->tail = reinterpret_cast<unsigned*>(ring_ptr + sq_off->tail);
    sq->ring_mask = reinterpret_cast<unsigned*>(ring_ptr + sq_off->ring_mask);
    sq->ring_entries = reinterpret_cast<unsigned*>(ring_ptr + sq_off->ring_entries);
    sq->flags = reinterpret_cast<unsigned*>(ring_ptr + sq_off->flags);
    sq->dropped = reinterpret_cast<unsigned*>(ring_ptr + sq_off->dropped);
    sq->array = reinterpret_cast<unsigned*>(ring_ptr + sq_off->array);

    size_t sqes_size = params.sq_entries * sizeof(io_uring_sqe);
    sq->sqes = memory::mmap<io_uring_sqe>(nullptr, sqes_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_POPULATE, fd, IORING_OFF_SQES);
    if (sq->sqes == nullptr) {
        return 0;
    }

    // completion queue setup

    io_cqring_offsets* cq_off = &params.cq_off;
    CQ* cq = &io_uring->cq;

    cq->head = reinterpret_cast<unsigned*>(ring_ptr + cq_off->head);
    cq->tail = reinterpret_cast<unsigned*>(ring_ptr + cq_off->tail);
    cq->ring_mask = reinterpret_cast<unsigned*>(ring_ptr + cq_off->ring_mask);
    cq->ring_entries = reinterpret_cast<unsigned*>(ring_ptr + cq_off->ring_entries);
    cq->overflow = reinterpret_cast<unsigned*>(ring_ptr + cq_off->overflow);
    cq->cqes = reinterpret_cast<io_uring_cqe*>(ring_ptr + cq_off->cqes);
    cq->flags = reinterpret_cast<unsigned*>(ring_ptr + cq_off->flags);

    return fd;
}

void clean(IoUring* io_uring) {
    memory::munmap(io_uring->sq.sqes, *io_uring->sq.ring_entries * sizeof(io_uring_sqe));
    memory::munmap(io_uring->ring_ptr, io_uring->ring_size);
    close(io_uring->fd);
}

/* SYSCALLS */

FAYE_INLINE ssize_t enter(IoUring* io_uring, unsigned to_submit, unsigned min_complete, unsigned flags, sigset_t* sig) {
    return io_uring_enter(io_uring->fd, to_submit, min_complete, flags, sig, sizeof(sigset_t));
}

FAYE_INLINE ssize_t register_fd(IoUring* io_uring, unsigned opcode, void* arg, unsigned nr_args) {
    return io_uring_register(io_uring->fd, opcode, arg, nr_args);
}

/* COMPLETION QUEUE */

FAYE_INLINE unsigned get_cqe_nr(IoUring* io_uring) {
    CQ* cq = &io_uring->cq;
    unsigned tail = atomic::load_acquire(cq->tail);

    return tail - *cq->head;
}

FAYE_INLINE io_uring_cqe* get_cqe(IoUring* io_uring) {
    CQ* cq = &io_uring->cq;
    return &cq->cqes[*cq->head & *cq->ring_mask];
}

FAYE_INLINE void mark_cqe_complete(IoUring* io_uring) {
    CQ* cq = &io_uring->cq;
    unsigned head = *cq->head;
    atomic::store_release(cq->head, head + 1);
}

/* SUBMISSION QUEUE */

FAYE_INLINE bool is_sq_full(IoUring* io_uring) {
    SQ* sq = &io_uring->sq;
    unsigned head = atomic::load_acquire(sq->head);

    return *sq->tail - head >= *sq->ring_entries;
}

FAYE_INLINE io_uring_sqe* get_sqe(IoUring* io_uring, uint8_t iosqe_flags, uint16_t ioprio, uint64_t user_data) {
    SQ* sq = &io_uring->sq;

    // get a sqe
    io_uring_sqe* sqe = &sq->sqes[*sq->tail & *sq->ring_mask];

    // initialize it
    sqe->flags = iosqe_flags;
    sqe->ioprio = ioprio;
    sqe->fd = -1;
    sqe->off = 0;
    sqe->addr = reinterpret_cast<uint64_t>(nullptr);
    sqe->len = 0;
    sqe->rw_flags = 0;
    sqe->user_data = user_data;
    sqe->__pad2[0] = 0;
    sqe->__pad2[1] = 0;
    sqe->__pad2[2] = 0;

    return sqe;
}

FAYE_INLINE void submit(IoUring* io_uring) {
    SQ* sq = &io_uring->sq;

    unsigned tail = *sq->tail;
    unsigned mask = *sq->ring_mask;
    unsigned idx = tail & mask;
    sq->array[idx] = idx;

    atomic::store_release(sq->tail, *sq->tail + 1);
}

FAYE_INLINE ssize_t wakeup(IoUring* io_uring, unsigned nr) {
    SQ* sq = &io_uring->sq;

    if (FAYE_UNLIKELY(*sq->flags & IORING_SQ_NEED_WAKEUP)) {
        return enter(io_uring, nr, 0, IORING_ENTER_SQ_WAKEUP | IORING_ENTER_SQ_WAIT, nullptr);
    }

    return nr;
}

/* SQE HELPERS */

FAYE_INLINE void nop(io_uring_sqe* sqe) {
    sqe->opcode = IORING_OP_NOP;
}

FAYE_INLINE void readv(io_uring_sqe* sqe, int fd, const iovec* iov, int iovcnt, off_t offset) {
    sqe->opcode = IORING_OP_READV;
    sqe->fd = fd;
    sqe->off = offset;
    sqe->addr = reinterpret_cast<uint64_t>(iov);
    sqe->len = iovcnt;
}

FAYE_INLINE void writev(io_uring_sqe* sqe, int fd, const iovec* iov, int iovcnt, off_t offset) {
    sqe->opcode = IORING_OP_WRITEV;
    sqe->fd = fd;
    sqe->off = offset;
    sqe->addr = reinterpret_cast<uint64_t>(iov);
    sqe->len = iovcnt;
}

FAYE_INLINE void fsync(io_uring_sqe* sqe, int fd, unsigned flags) {
    sqe->opcode = IORING_OP_FSYNC;
    sqe->fd = fd;
    sqe->fsync_flags = flags;
}

FAYE_INLINE void read_fixed(io_uring_sqe* sqe, int fd, const char* buf, size_t count, off_t offset, uint16_t buf_index) {
    sqe->opcode = IORING_OP_READ_FIXED;
    sqe->fd = fd;
    sqe->off = offset;
    sqe->addr = reinterpret_cast<uint64_t>(buf);
    sqe->len = count;
    sqe->buf_index = buf_index;
}

FAYE_INLINE void write_fixed(io_uring_sqe* sqe, int fd, const char* buf, size_t count, off_t offset, uint16_t buf_index) {
    sqe->opcode = IORING_OP_WRITE_FIXED;
    sqe->fd = fd;
    sqe->off = offset;
    sqe->addr = reinterpret_cast<uint64_t>(buf);
    sqe->len = count;
    sqe->buf_index = buf_index;
}

FAYE_INLINE void poll_add(io_uring_sqe* sqe, int fd, unsigned events) {
    sqe->opcode = IORING_OP_POLL_ADD;
    sqe->fd = fd;
#if __BYTE_ORDER == __BIG_ENDIAN
    sqe->poll32_events = __swahw32(events);
#else
    sqe->poll32_events = events;
#endif
}

FAYE_INLINE void poll_remove(io_uring_sqe* sqe, void* user_data) {
    sqe->opcode = IORING_OP_POLL_REMOVE;
    sqe->addr = reinterpret_cast<uint64_t>(user_data); // data must match the poll_add user_data field to cancel
}

FAYE_INLINE void sync_file_range(io_uring_sqe* sqe, int fd, off_t offset, off_t nbytes, unsigned flags) {
    sqe->opcode = IORING_OP_SYNC_FILE_RANGE;
    sqe->fd = fd;
    sqe->off = offset;
    sqe->len = nbytes;
    sqe->sync_range_flags = flags;
}

FAYE_INLINE void sendmsg(io_uring_sqe* sqe, int fd, const msghdr* msg, unsigned flags) {
    sqe->opcode = IORING_OP_SENDMSG;
    sqe->fd = fd;
    sqe->addr = reinterpret_cast<uint64_t>(msg);
    sqe->len = 1;
    sqe->msg_flags = flags;
}

FAYE_INLINE void recvmsg(io_uring_sqe* sqe, int fd, msghdr* msg, unsigned flags) {
    sqe->opcode = IORING_OP_RECVMSG;
    sqe->fd = fd;
    sqe->addr = reinterpret_cast<uint64_t>(msg);
    sqe->len = 1;
    sqe->msg_flags = flags;
}

FAYE_INLINE void timeout(io_uring_sqe* sqe, timespec* ts, unsigned count, unsigned flags) {
    sqe->opcode = IORING_OP_TIMEOUT;
    sqe->off = count;
    sqe->addr = reinterpret_cast<uint64_t>(ts);
    sqe->len = 1;
    sqe->timeout_flags = flags;
}

FAYE_INLINE void timeout_remove(io_uring_sqe* sqe, void* user_data, unsigned flags) {
    sqe->opcode = IORING_OP_TIMEOUT_REMOVE;
    sqe->addr = reinterpret_cast<uint64_t>(user_data);
    sqe->timeout_flags = flags;
}

FAYE_INLINE void accept(io_uring_sqe* sqe, int fd, sockaddr* addr, const int* addrlen, unsigned flags) {
    sqe->opcode = IORING_OP_ACCEPT;
    sqe->fd = fd;
    sqe->addr2 = reinterpret_cast<uint64_t>(addrlen);
    sqe->addr = reinterpret_cast<uint64_t>(addr);
    sqe->accept_flags = flags;
}

FAYE_INLINE void async_cancel(io_uring_sqe* sqe, void* user_data, unsigned flags) {
    sqe->opcode = IORING_OP_ASYNC_CANCEL;
    sqe->addr = reinterpret_cast<uint64_t>(user_data);
    sqe->cancel_flags = flags;
}

FAYE_INLINE void link_timeout(io_uring_sqe* sqe, timespec* ts, unsigned flags) {
    sqe->opcode = IORING_OP_LINK_TIMEOUT;
    sqe->addr = reinterpret_cast<uint64_t>(ts);
    sqe->len = 1;
    sqe->timeout_flags = flags;
}

FAYE_INLINE void connect(io_uring_sqe* sqe, int fd, const sockaddr* addr, int addrlen) {
    sqe->opcode = IORING_OP_CONNECT;
    sqe->fd = fd;
    sqe->off = addrlen;
    sqe->addr = reinterpret_cast<uint64_t>(addr);
}

FAYE_INLINE void fallocate(io_uring_sqe* sqe, int fd, int mode, off_t offset, off_t len) {
    sqe->opcode = IORING_OP_FALLOCATE;
    sqe->fd = fd;
    sqe->off = offset;
    sqe->addr = len;
    sqe->len = mode;
}

FAYE_INLINE void openat(io_uring_sqe* sqe, int dirfd, const char* path, unsigned flags, mode_t mode) {
    sqe->opcode = IORING_OP_OPENAT;
    sqe->fd = dirfd;
    sqe->addr = reinterpret_cast<uint64_t>(path);
    sqe->len = mode;
    sqe->open_flags = flags;
}

FAYE_INLINE void close(io_uring_sqe* sqe, int fd) {
    sqe->opcode = IORING_OP_CLOSE;
    sqe->fd = fd;
}

FAYE_INLINE void files_update(io_uring_sqe* sqe, const int* fds, unsigned nr_fds, off_t offset) {
    sqe->opcode = IORING_OP_FILES_UPDATE;
    sqe->off = offset;
    sqe->addr = reinterpret_cast<uint64_t>(fds);
    sqe->len = nr_fds;
}

FAYE_INLINE void statx(io_uring_sqe* sqe, int dirfd, const char* path, unsigned flags, unsigned mask, struct statx* statxbuf) {
    sqe->opcode = IORING_OP_STATX;
    sqe->fd = dirfd;
    sqe->addr2 = reinterpret_cast<uint64_t>(statxbuf);
    sqe->addr = reinterpret_cast<uint64_t>(path);
    sqe->len = mask;
    sqe->statx_flags = flags;
}

FAYE_INLINE void read(io_uring_sqe* sqe, int fd, const char* buf, size_t count, off_t offset) {
    sqe->opcode = IORING_OP_READ;
    sqe->fd = fd;
    sqe->off = offset;
    sqe->addr = reinterpret_cast<uint64_t>(buf);
    sqe->len = count;
}

FAYE_INLINE void write(io_uring_sqe* sqe, int fd, const char* buf, size_t count, off_t offset) {
    sqe->opcode = IORING_OP_WRITE;
    sqe->fd = fd;
    sqe->off = offset;
    sqe->addr = reinterpret_cast<uint64_t>(buf);
    sqe->len = count;
}

FAYE_INLINE void fadvise(io_uring_sqe* sqe, int fd, off_t offset, off_t len, int advice) {
    sqe->opcode = IORING_OP_FADVISE;
    sqe->fd = fd;
    sqe->off = offset;
    sqe->len = len;
    sqe->fadvise_advice = advice;
}

FAYE_INLINE void madvise(io_uring_sqe* sqe, void* addr, size_t len, int advice) {
    sqe->opcode = IORING_OP_MADVISE;
    sqe->addr = reinterpret_cast<uint64_t>(addr);
    sqe->len = len;
    sqe->fadvise_advice = advice;
}

FAYE_INLINE void send(io_uring_sqe* sqe, int fd, const void* buf, size_t len, unsigned flags) {
    sqe->opcode = IORING_OP_SEND;
    sqe->fd = fd;
    sqe->addr = reinterpret_cast<uint64_t>(buf);
    sqe->len = len;
    sqe->msg_flags = flags;
}

FAYE_INLINE void recv(io_uring_sqe* sqe, int fd, void* buf, size_t len, unsigned flags) {
    sqe->opcode = IORING_OP_RECV;
    sqe->fd = fd;
    sqe->off = 0;
    sqe->addr = reinterpret_cast<uint64_t>(buf);
    sqe->len = len;
    sqe->msg_flags = flags;
}

FAYE_INLINE void openat2(io_uring_sqe* sqe, int fd, const char* path, open_how* how) {
    sqe->opcode = IORING_OP_OPENAT2;
    sqe->fd = fd;
    sqe->addr2 = reinterpret_cast<uint64_t>(how);
    sqe->addr = reinterpret_cast<uint64_t>(path);
    sqe->len = sizeof(open_how);
}

FAYE_INLINE void epoll_ctl(io_uring_sqe* sqe, int epfd, int op, int fd, epoll_event* ev) {
    sqe->opcode = IORING_OP_EPOLL_CTL;
    sqe->fd = epfd;
    sqe->off = fd;
    sqe->addr = reinterpret_cast<uint64_t>(ev);
    sqe->len = op;
}

FAYE_INLINE void splice(io_uring_sqe* sqe, int fd_in, int32_t off_in, int fd_out, int64_t off_out, unsigned nbytes, unsigned splice_flags) {
    sqe->opcode = IORING_OP_SPLICE;
    sqe->fd = fd_out;
    sqe->off = off_out;
    sqe->len = nbytes;
    sqe->splice_fd_in = off_in;
    sqe->splice_fd_in = fd_in;
    sqe->splice_flags = splice_flags;
}

FAYE_INLINE void provide_buffers(io_uring_sqe* sqe, void* addr, int len, int nr, int bgid, int bid) {
    sqe->opcode = IORING_OP_PROVIDE_BUFFERS;
    sqe->fd = nr;
    sqe->off = bid;
    sqe->addr = reinterpret_cast<uint64_t>(addr);
    sqe->len = len;
    sqe->buf_group = bgid;
}

FAYE_INLINE void remove_buffers(io_uring_sqe* sqe, int nr, int bgid) {
    sqe->opcode = IORING_OP_REMOVE_BUFFERS;
    sqe->fd = nr;
    sqe->buf_group = bgid;
}

FAYE_INLINE void tee(io_uring_sqe* sqe, int fd_in, int fd_out, unsigned nbytes, unsigned splice_flags) {
    sqe->opcode = IORING_OP_TEE;
    sqe->fd = fd_out;
    sqe->len = nbytes;
    sqe->splice_fd_in = 0;
    sqe->splice_fd_in = fd_in;
    sqe->splice_flags = splice_flags;
}

} // namespace faye::io_uring
