#pragma once

#include <linux/futex.h>

#include "atomic.h"
#include "syscall.h"

namespace faye {

using Lock = uint32_t;

} // namespace faye

namespace faye::lock {

/* FUTEXES */

ssize_t futex_wait(Lock* lock, uint32_t val) {
    return futex(lock, FUTEX_WAIT_PRIVATE, val, nullptr, nullptr, 0);
}

ssize_t futex_wake(Lock* lock, uint32_t waken_nr) {
    return futex(lock, FUTEX_WAKE_PRIVATE, waken_nr, nullptr, nullptr, 0);
}

ssize_t futex_requeue(Lock* lock_from, Lock* lock_to, uint32_t val, uint32_t requeued_nr, __kernel_timespec* requeued_limit) {
    return futex(lock_from, FUTEX_CMP_REQUEUE_PRIVATE, requeued_nr, requeued_limit, lock_to, val);
}

/* SPINLOCKS */

uint32_t spinlock_wait(Lock* lock, uint32_t expected, uint32_t desired) {
    while (!atomic::cmpxchg(lock, expected, desired)) {
        asm volatile("" ::: "memory");
    }

    return *lock;
}

} // namespace faye::lock
