#pragma once

#include <linux/mman.h>

#include "syscall.h"

namespace faye::memory {

template<typename T = void>
T* mmap(void* addr, size_t len, int prot, unsigned flags, int fd, off_t off) {
    // clang-format off
    // NOLINTNEXTLINE (performance-no-int-to-ptr)
    return reinterpret_cast<T*>(
        ::mmap(reinterpret_cast<unsigned long>(addr), len, prot, flags, fd, off)
    );
    // clang-format on
}

ssize_t munmap(void* addr, size_t len) {
    return ::munmap(reinterpret_cast<unsigned long>(addr), len);
}

/* WRAPPERS */

struct Allocation {
    size_t size;
    char mem[];
};

template<typename T = void>
T* malloc(size_t size) {
    // clang-format off
    // NOLINTNEXTLINE (performance-no-int-to-ptr)
    auto alloc = reinterpret_cast<Allocation*>(
        ::mmap(0, sizeof(Allocation) + size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)
    );
    // clang-format on

    alloc->size = size;

    return reinterpret_cast<T*>(alloc->mem);
}

ssize_t free(void* item) {
    // clang-format off
    auto alloc = reinterpret_cast<Allocation*>(
        reinterpret_cast<char*>(item) - sizeof(Allocation)
    );
    // clang-format on

    return ::munmap(reinterpret_cast<unsigned long>(alloc), alloc->size);
}

} // namespace faye::memory