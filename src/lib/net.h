#pragma once

#include <stdint.h>

#include "net_types.h"
#include "utils.h"

namespace faye::net {

/* IP PARSING */

int inet_pton4(const char* src, unsigned char* __restrict dest) {
    constexpr unsigned too_many_chars_in_field = 4;
    constexpr size_t dest_size = 4;

    unsigned char* dest_end = dest + dest_size;
    size_t written = 0;

    for (char c = *src; c != '\0'; src++, c = *src) {
        if (c == '.') {
            dest++;
            if (dest == dest_end) {
                return 0;
            }

            written = 0;
        } else if (c < '0' || c > '9') {
            return 0;
        } else {
            written++;
            if (written == too_many_chars_in_field) {
                return 0;
            }

            *dest *= 10;
            *dest += c - '0';
        }
    }

    return 1;
}

int inet_pton6(const char* src, unsigned char* __restrict dest) {
    constexpr unsigned too_many_chars_in_field = 5;
    constexpr size_t dest_size = 8;

    auto dest_16 = reinterpret_cast<uint16_t* __restrict>(dest);
    uint16_t* dest_16_end = dest_16 + dest_size;
    bool colon_found = false;
    uint16_t* dest_16_forward_end = nullptr;
    size_t written = 0;

    for (char c = *src; c != '\0'; src++, c = *src) {
        if (c == ':') {
            if (colon_found) {
                if (dest_16_forward_end != nullptr) {
                    // if we've already found a "::" and we find another the
                    // address is ill formed
                    return 0;
                }

                // if it's the first "::" we find then store the location of
                // the corresponding dest entry
                dest_16_forward_end = dest_16;
            } else {
                // if we find a simple ":" then we move the dest pointer and
                // record the fact
                dest_16++;
                if (dest_16 == dest_16_end) {
                    return 0;
                }
                colon_found = true;
            }

            written = 0;
        } else {
            // record we've not found a ":"
            colon_found = false;

            // convert the char and write it if it's valid
            c = utils::char2hex(c);
            if (c == -1) {
                return 0;
            }

            written++;
            if (written == too_many_chars_in_field) {
                return 0;
            }

            *dest_16 *= 16;
            *dest_16 += c;
        }
    }

    // copy the shorts after the "::" in the correct position
    if (dest_16_forward_end != nullptr) {
        dest_16_end--;
        for (; dest_16_forward_end <= dest_16; dest_16--, dest_16_end--) {
            *dest_16_end = *dest_16;
            *dest_16 = 0;
        }
    }

    return 1;
}

} // namespace faye::net
