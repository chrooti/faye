#pragma once

#include <linux/sched.h>

#include "syscall.h"
#include "utils.h"

// glibc/posix/bits/cpu-set.h
using cpu_set_t = struct { size_t bits[1024 / 8 * sizeof(size_t)]; };

namespace faye::process {

/* THREADS */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

// see sp_offset and struct zeroing for this
static_assert(sizeof(clone_args) + 8 == 96);

// clang-format off
template<typename T>
// NOLINTNEXTLINE (misc-unused-parameters)
__attribute__((__naked__)) pid_t spawn_thread(void (*func)(T arg), T arg, void* stack, size_t stack_size) {
    // func: %rdi
    // arg: %rsi
    // stack: %rdx
    // stack_size: %rcx

    asm volatile(
        // stack composition: see the sp_offset
        "subq %[sp_offset], %%rsp\n"

        // move func to the top stack frame
        "mov %%rdi, -8(%%rdx, %%rcx)\n"

        // move func param to 4th frame (see below)

        // calc unreserved stack size
        "leaq -8(%%rcx), %%rax\n"

        // zero xmm0 and zero teh struct 16 bytes at a time
        "xorps %%xmm0, %%xmm0\n"
        "movups %%xmm0, (%%rsp)\n"
        "movups %%xmm0, 16(%%rsp)\n"
        "movups %%xmm0, 32(%%rsp)\n"
        // skip 48, it's stack and stack size (see below)
        "movups %%xmm0, 64(%%rsp)\n"
        "movups %%xmm0, 80(%%rsp)\n"

        // set up the parameters
        "movl %[flags],          %P[flags_off](%%rsp)\n"
        "movq %[exit_signal],    %P[exit_signal_off](%%rsp)\n"
        "movq %%rdx,             %P[stack_off](%%rsp)\n"
        "movq %%rax,             %P[stack_size_off](%%rsp)\n"

        // push the jump address for the main function on stack
        "leaq .caller_after_clone(%%rip), %%rax\n"
        "pushq %%rax\n"

        // save the arg
        "mov %%rsi, %%r8\n"

        // invoke the clone3 syscall
        "mov %[clone3_nr], %%rax\n"
        "leaq 8(%%rsp), %%rdi\n"
        "mov %[clone_args_size], %%rsi\n"
        "syscall\n"

        // put the arg where the thread will find it
        "mov %%r8, %%rdi\n"

        // jump
        // caller: will go to caller_after_clone
        // new thread: will jump to the func param
        "ret\n"

        // caller: set the stack pointer and return
        ".caller_after_clone:\n"
        "addq %[sp_offset], %%rsp\n"
        "ret\n"
        :
        :
        // stack composition:
        // - sizeof(clone_args): clone_args struct
        [sp_offset] "i"(sizeof(clone_args) + 8),

        [flags] "i"(0L | CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_PARENT | CLONE_THREAD | CLONE_IO),
        [flags_off] "i"(offsetof(clone_args, flags)),
        [exit_signal] "i"(0), [exit_signal_off] "i"(offsetof(clone_args, exit_signal)),
        [stack_off] "i"(offsetof(clone_args, stack)),
        [stack_size_off] "i"(offsetof(clone_args, stack_size)),

        [clone3_nr] "i"(__NR_clone3), [clone_args_size] "i"(sizeof(struct clone_args))
        : "rcx", "r11");
}
// clang-format on

#pragma GCC diagnostic pop

FAYE_INLINE void cpu_set_idx(cpu_set_t* set, unsigned idx) {
    size_t bits_per_long = (sizeof(size_t) * 8);

    size_t cpu_group = idx / bits_per_long;
    size_t cpu_idx = idx % bits_per_long;

    set->bits[cpu_group] = set->bits[cpu_group] | (1ULL << cpu_idx);
}

} // namespace faye::process
