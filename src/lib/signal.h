#pragma once

#include "syscall.h"

namespace faye::signal {

extern "C" {
// clang-format off
__attribute__((__naked__, __noinline__)) void rt_sigreturn() {
    asm(
        "movq $15, %rax\n"
        "syscall\n"
    );
}
// clang-format on
}

ssize_t sigaction_rt(int sig, __sighandler_t handler, unsigned flags, sigset_t mask, sigaction* oact) {
    sigaction act = {
        .sa_handler = handler,
        .sa_flags = flags | SA_RESTORER,
        .sa_restorer = &rt_sigreturn,
        .sa_mask = mask,
    };

    return rt_sigaction(sig, &act, oact, sizeof(sigset_t));
}

} // namespace faye::signal
