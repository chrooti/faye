#pragma once

#include "io.h"
#include "utils.h"

namespace faye::sysinfo {

unsigned get_nprocs_conf() {
    // I hope max thread count is four digits long at most so "xxxx-xxxx" is 9 chars long
    constexpr unsigned max_procs_digits = 4;
    constexpr unsigned max_nprocs_size = 2 * max_procs_digits + 1;

    char buf[max_nprocs_size];
    ssize_t read_size = io::read_file("/sys/devices/system/cpu/online", buf, max_nprocs_size);
    if (read_size < 0) {
        return 1;
    }

    for (char *ptr = buf, *ptr_end = buf + read_size / 2 + 1; ptr != ptr_end; ptr++) {
        if (*ptr == '-') {
            return utils::dec2int(ptr + 1, buf + read_size);
        }
    }

    return 1;
}

} // namespace faye::sysinfo
