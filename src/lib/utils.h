#pragma once

#include <stddef.h>

#define FAYE_INLINE [[maybe_unused]] __attribute__((__always_inline__)) static inline

#define FAYE_LIKELY(x) __builtin_expect((x), 1)
#define FAYE_UNLIKELY(x) __builtin_expect((x), 0)

namespace faye::utils {

FAYE_INLINE size_t dec2int(char* buf, const char* buf_end) {
    size_t num = *buf - '0';
    buf++;

    for (; buf != buf_end - 1; buf++) {
        num *= 10;
        num += *buf - '0';
    }
    return num;
}

FAYE_INLINE char char2hex(char c) {
    char ch = c - '0'; // NOLINT (cppcoreguidelines-narrowing-conversions)
    if (ch < 10) {
        return ch;
    }

    c |= 32;
    ch = c - 'a'; // NOLINT (cppcoreguidelines-narrowing-conversions)
    if (ch < 6) {
        return ch + 10; // NOLINT (cppcoreguidelines-narrowing-conversions)
    }

    return -1;
}

} // namespace faye::utils