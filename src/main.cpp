#include "http/ssl_server.h"
#include "http/http2.h"
#include "server/api.h"
#include "server/executor.h"

//#include "http/ssl_types.h"
//#include "http/ssl.h"
//#include "template/template.h"

/* temporaries */

// clang-format off

#define LC_START(s) switch (s) { case 0:

#define LC_AWAIT(s) s = __LINE__; return; case __LINE__:

#define LC_END(s) s = 0; }

// clang-format on

const char default_res[] = "HTTP/1.1 200 OK\r\n"
                           "Content-Length: 2\r\n"
                           "Content-Type: text/html\r\n"
                           "Connection: Closed\r\n\r\n"
                           "ao";

struct CustomConnection {
    faye::BaseConnection base;

    uint16_t pt;
    utter::Buffer<8192> req_buf;
};

struct Callbacks {
    FAYE_INLINE void on_empty_pool(faye::Worker<CustomConnection>* /* worker */) {
        auto out = utter::output();
        utter::to(out, "empty pool\n", utter::to_stdout);
    }

    FAYE_INLINE void on_request(faye::Worker<CustomConnection>* worker, uint64_t data) {
        CustomConnection* conn = faye::get_conn(worker, data);
        auto out = utter::output();

        io_uring_sqe* sqe;
        faye::ssl::Context ssl_ctx;
        faye::http2::Request req;
        faye::http2::Response res;

        ssize_t ret;
        char plain_req_buf[8192];

        LC_START(conn->pt)

        // check if accept worked and log
        if (conn->base.fd < 0) {
            faye::errors::log_syscall(out, "accept", conn->base.fd);
            faye::abort_conn(worker, data);
            conn->pt = 0;
            return;
        }

        sqe = faye::get_sqe(worker, data, 0, 0);
        faye::recv(worker, sqe, conn->req_buf.data, 8192, 0);
        faye::wakeup(worker, 1);
        LC_AWAIT(conn->pt)

        conn->req_buf.len = conn->base.last_ret;

        faye::ssl::read(
            &ssl_ctx,
            reinterpret_cast<const uint8_t*>(conn->req_buf.data),
            reinterpret_cast<const uint8_t*>(conn->req_buf.data + conn->req_buf.len)
        );

//        faye::http2::request::read(&req, )
        // do processing


        sqe = faye::get_sqe(worker, data, 0, 0);
        faye::send(worker, sqe, default_res, sizeof(default_res) - 1, 0);
        faye::wakeup(worker, 1);
        LC_AWAIT(conn->pt)

        faye::close_conn(worker, data);
        LC_END(conn->pt)
    }
};

// types
//template<typename Connection>
//using OnEmptyPool = void(Worker<Connection>* worker);
//
//template<typename Connection>
//using OnRequest = void(Worker<Connection>* worker, uint64_t conn);
//

void build_sockaddr_v4(sockaddr_in* addr, const char* ip, unsigned short port) {
    addr->sin_family = AF_INET;

#if __BYTE_ORDER == __BIG_ENDIAN
    addr->sin_port = __builtin_bswap16(port);
#else
    addr->sin_port = port;
#endif

    faye::net::inet_pton4(ip, reinterpret_cast<unsigned char*>(&addr->sin_addr));
}

extern "C" {
int main() {
    faye::Config config = {
        .conn_per_thread_nr = 4200,
        .thread_nr = 0,
        .loop_timeout_delay = 10,
        .loop_timeout_duration =
            {
                .tv_sec = 1,
                .tv_nsec = 0,
            },
        .addr_size = sizeof(sockaddr_in),
    };
    build_sockaddr_v4(&config.addr.v4, "127.0.0.1", 8180);

    auto executor = faye::executor::create<CustomConnection>(&config);
    if (executor == nullptr) {
        return -1;
    }

    // drop cap_sys_nice here, if some user code needs it we can't do it safely

    if (faye::executor::start<Callbacks>(executor) != 0) {
        return -1;
    }

//    utter::BufferView packet;
//    unsigned char packet_buf[8192];
//    ssize_t r = faye::io::read_file("../test/clienthello", reinterpret_cast<char*>(packet_buf), 8192);
//
//    auto buf_start = packet_buf;
//    auto buf_end = packet_buf + r;
//
//    auto out = utter::output();
//    utter::to(out, "buf len: ", utter::int2dec(r), "\n", utter::to_stdout);
//
//    r = (ssize_t) faye::ssl::parse_tls_plaintext(
//        buf_start,
//        buf_end
//    );

    return 0;
}
}