#pragma once

#include "worker.h"

namespace faye {

/* DATA UTILITIES */

namespace internal {

FAYE_INLINE uint32_t get_conn_idx(uint64_t data) {
    return static_cast<uint32_t>(data);
}

FAYE_INLINE uint64_t get_next_data(uint64_t data) {
    return data + (1ULL << 32);
}

} // namespace internal

template<typename Connection>
FAYE_INLINE Connection* get_conn(Worker<Connection>* worker, uint64_t data) {
    uint32_t conn_idx = internal::get_conn_idx(data);
    return connection::get(&worker->conn_pool, conn_idx);
}

FAYE_INLINE uint32_t get_io_idx(uint64_t data) {
    return data >> 32;
}

/* SUBMISSION QUEUE */

template<typename Connection>
FAYE_INLINE io_uring_sqe* get_sqe(Worker<Connection>* worker, uint64_t data, uint8_t iosqe_flags, uint16_t ioprio) {
    return io_uring::get_sqe(&worker->io_uring, iosqe_flags, ioprio, internal::get_next_data(data));
}

template<typename Connection>
FAYE_INLINE ssize_t wakeup(Worker<Connection>* worker, unsigned nr) {
    return io_uring::wakeup(&worker->io_uring, nr);
}

template<typename Connection>
FAYE_INLINE ssize_t register_fd(Worker<Connection>* worker, unsigned opcode, void* arg, unsigned nr_args) {
    return io_uring::register_fd(&worker->io_uring, opcode, arg, nr_args);
}

/* SERVER LAYER */

// called when the socket hasn't been opened (e.g. accept error)
template<typename Connection>
FAYE_INLINE void abort_conn(Worker<Connection>* worker, uint64_t data) {
    ConnectionPool<Connection>* conn_pool = &worker->conn_pool;
    uint32_t conn_idx = internal::get_conn_idx(data);

    // put back the conn and queue an accept if needed
    connection::put_back(conn_pool, conn_idx);
    if (FAYE_UNLIKELY(!worker->is_accepting)) {
        worker->is_accepting = true;
        worker::accept(worker);
    }
}

template<typename Connection>
FAYE_INLINE ssize_t close_conn(Worker<Connection>* worker, uint64_t data) {
    IoUring* io_uring = &worker->io_uring;
    ConnectionPool<Connection>* conn_pool = &worker->conn_pool;

    uint32_t conn_idx = internal::get_conn_idx(data);
    Connection* conn = connection::get(conn_pool, conn_idx);
    auto base_conn = reinterpret_cast<BaseConnection*>(conn);

    abort_conn(worker, data);

    io_uring_sqe* sqe = io_uring::get_sqe(io_uring, 0, 0, max_conn_idx);
    io_uring::close(sqe, base_conn->fd);
    io_uring::submit(io_uring);
    return io_uring::wakeup(io_uring, 1);
}

template<typename Connection>
FAYE_INLINE uint64_t sendmsg(Worker<Connection>* worker, io_uring_sqe* sqe, const msghdr* msg, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;
    Connection* conn = get_conn(worker, sqe->user_data);

    io_uring::sendmsg(sqe, conn->fd, msg, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
FAYE_INLINE uint64_t recvmsg(Worker<Connection>* worker, io_uring_sqe* sqe, msghdr* msg, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;
    Connection* conn = get_conn(worker, sqe->user_data);
    auto base_conn = reinterpret_cast<BaseConnection*>(conn);

    io_uring::recvmsg(sqe, base_conn->fd, msg, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
FAYE_INLINE uint64_t send(Worker<Connection>* worker, io_uring_sqe* sqe, const void* buf, size_t len, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;
    Connection* conn = get_conn(worker, sqe->user_data);
    auto base_conn = reinterpret_cast<BaseConnection*>(conn);

    io_uring::send(sqe, base_conn->fd, buf, len, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
FAYE_INLINE uint64_t recv(Worker<Connection>* worker, io_uring_sqe* sqe, void* buf, size_t len, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;
    Connection* conn = get_conn(worker, sqe->user_data);
    auto base_conn = reinterpret_cast<BaseConnection*>(conn);

    io_uring::recv(sqe, base_conn->fd, buf, len, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

/* USER LAYER */

template<typename Connection>
[[maybe_unused]] uint64_t readv(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, const iovec* iov, int iovcnt, off_t offset) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::readv(sqe, fd, iov, iovcnt, offset);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t writev(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, const iovec* iov, int iovcnt, off_t offset) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::writev(sqe, fd, iov, iovcnt, offset);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t fsync(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::fsync(sqe, fd, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t read_fixed(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, const char* buf, size_t count, off_t offset, uint16_t buf_index) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::read_fixed(sqe, fd, buf, count, offset, buf_index);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t write_fixed(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, const char* buf, size_t count, off_t offset, uint16_t buf_index) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::write_fixed(sqe, fd, buf, count, offset, buf_index);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t sync_file_range(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, off_t offset, off_t nbytes, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::sync_file_range(sqe, fd, offset, nbytes, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t timeout(Worker<Connection>* worker, io_uring_sqe* sqe, timespec* ts, unsigned count, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::timeout(sqe, ts, count, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t timeout_remove(Worker<Connection>* worker, io_uring_sqe* sqe, void* user_data, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::timeout_remove(sqe, user_data, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t async_cancel(Worker<Connection>* worker, io_uring_sqe* sqe, void* user_data, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::async_cancel(sqe, user_data, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t link_timeout(Worker<Connection>* worker, io_uring_sqe* sqe, timespec* ts, unsigned flags) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::link_timeout(sqe, ts, flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t connect(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, const sockaddr* addr, int addrlen) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::connect(sqe, fd, addr, addrlen);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t fallocate(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, int mode, off_t offset, off_t len) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::fallocate(sqe, fd, mode, offset, len);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t openat(Worker<Connection>* worker, io_uring_sqe* sqe, int dirfd, const char* path, unsigned flags, mode_t mode) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::openat(sqe, dirfd, path, flags, mode);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t close(Worker<Connection>* worker, io_uring_sqe* sqe, int fd) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::close(sqe, fd);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t files_update(Worker<Connection>* worker, io_uring_sqe* sqe, const int* fds, unsigned nr_fds, off_t offset) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::files_update(sqe, fds, nr_fds, offset);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t statx(Worker<Connection>* worker, io_uring_sqe* sqe, int dirfd, const char* path, unsigned flags, unsigned mask, struct statx* statxbuf) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::statx(sqe, dirfd, path, flags, mask, statxbuf);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t read(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, const char* buf, size_t count, off_t offset) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::read(sqe, fd, buf, count, offset);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t write(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, const char* buf, size_t count, off_t offset) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::write(sqe, fd, buf, count, offset);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t fadvise(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, off_t offset, off_t len, int advice) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::fadvise(sqe, fd, offset, len, advice);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t madvise(Worker<Connection>* worker, io_uring_sqe* sqe, void* addr, size_t len, int advice) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::madvise(sqe, addr, len, advice);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t openat2(Worker<Connection>* worker, io_uring_sqe* sqe, int fd, const char* path, open_how* how) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::openat2(sqe, fd, path, how);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t splice(Worker<Connection>* worker, io_uring_sqe* sqe, int fd_in, int32_t off_in, int fd_out, int64_t off_out, unsigned nbytes, unsigned splice_flags) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::splice(sqe, fd_in, off_in, fd_out, off_out, nbytes, splice_flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t provide_buffers(Worker<Connection>* worker, io_uring_sqe* sqe, void* addr, int len, int nr, int bgid, int bid) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::provide_buffers(sqe, addr, len, nr, bgid, bid);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t remove_buffers(Worker<Connection>* worker, io_uring_sqe* sqe, int nr, int bgid) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::remove_buffers(sqe, nr, bgid);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

template<typename Connection>
[[maybe_unused]] uint64_t tee(Worker<Connection>* worker, io_uring_sqe* sqe, int fd_in, int fd_out, unsigned nbytes, unsigned splice_flags) {
    IoUring* io_uring = &worker->io_uring;

    io_uring::tee(sqe, fd_in, fd_out, nbytes, splice_flags);
    io_uring::submit(io_uring);

    return sqe->user_data;
}

} // namespace faye