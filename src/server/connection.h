#pragma once

#include <stddef.h>
#include <stdint.h>

#include <linux/in.h>
#include <linux/in6.h>

#include "../lib/utils.h"

namespace faye {

struct BaseConnection {
    int fd;
    union {
        sockaddr_in v4;
        sockaddr_in6 v6;
    } addr;
    int addrlen;
    bool is_open;
    size_t last_ret;
};

template<typename Connection>
struct ConnectionPool {
    uint32_t first_conn_idx;
    uint32_t conn_nr;

    Connection* conns;

    // conn_nr * uint32_t (indexes)
    // + conn_nr * Connection (actual connections)
    uint32_t conn_indexes[];
};

} // namespace faye

namespace faye::connection {

template<typename Connection>
constexpr size_t compute_pool_size(uint32_t conn_nr) {
    return sizeof(ConnectionPool<Connection>) + conn_nr * (sizeof(uint32_t) + sizeof(Connection));
}

template<typename Connection>
void init_pool(ConnectionPool<Connection>* pool, uint32_t conn_nr) {
    pool->first_conn_idx = 0;
    pool->conn_nr = conn_nr;
    pool->conns = reinterpret_cast<Connection*>(&pool->conn_indexes[conn_nr]);

    for (uint32_t i = 0; i < conn_nr; i++) {
        pool->conn_indexes[i] = i;
    }
}

template<typename Connection>
FAYE_INLINE Connection* get(ConnectionPool<Connection>* pool, uint32_t conn_idx) {
    return &pool->conns[conn_idx];
}

template<typename Connection>
FAYE_INLINE bool is_pool_empty(ConnectionPool<Connection>* pool) {
    return pool->first_conn_idx == pool->conn_nr;
}

template<typename Connection>
FAYE_INLINE uint32_t borrow(ConnectionPool<Connection>* pool) {
    uint32_t conn_idx = pool->conn_indexes[pool->first_conn_idx];
    pool->first_conn_idx++;
    return conn_idx;
}

template<typename Connection>
FAYE_INLINE void put_back(ConnectionPool<Connection>* pool, uint32_t conn_idx) {
    pool->first_conn_idx--;
    pool->conn_indexes[pool->first_conn_idx] = conn_idx;
}

} // namespace faye::connection
