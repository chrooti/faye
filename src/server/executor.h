#pragma once

#include <linux/filter.h>
#include <linux/resource.h>

#include "../lib/errors.h"
#include "../lib/memory.h"
#include "../lib/net.h"
#include "../lib/process.h"
#include "../lib/signal.h"
#include "../lib/sysinfo.h"

#include "worker.h"

namespace faye {

template<typename Connection>
struct Executor {
    Context ctx;

    // thread_nr * sizeof(Worker)
    // + thead_nr * stack_size
    Worker<Connection> workers[];
};

} // namespace faye

namespace faye::executor {

/* INITIALIZATION */

template<typename Connection>
void destroy(Executor<Connection>* executor) {
    memory::free(executor);
}

template<typename Connection>
Executor<Connection>* create(Config* config) {
    if (config->thread_nr == 0) {
        config->thread_nr = sysinfo::get_nprocs_conf() + 1;
    }

    // get the thread stack size
    rlimit64 stack_limit;
    ssize_t ret = prlimit64(0, RLIMIT_STACK, nullptr, &stack_limit);
    if (ret < 0) {
        errors::log_syscall(global_out, "prlimit64", ret);
        return nullptr;
    }

    // allocate memory
    size_t stack_size = stack_limit.rlim_cur;
    size_t worker_size = worker::compute_size<Connection>(config->conn_per_thread_nr);

    // clang-format off
    size_t alloc_size = sizeof(Executor<Connection>)
                        + config->thread_nr * (worker_size + stack_size);
    // clang-format on

    Executor<Connection>* executor = memory::malloc<Executor<Connection>>(alloc_size);
    if (executor == nullptr) {
        return nullptr;
    }

    // initialize the context
    Context* ctx = &executor->ctx;

    ctx->config = *config;
    ctx->is_stopping = false;
    ctx->is_first_worker_setup = false;
    ctx->worker_size = worker_size;
    ctx->stack_size = stack_size;
    atomic::store_release(&ctx->workers_ready_lock, config->thread_nr);
    atomic::store_release(&ctx->workers_cleanup_lock, 0U);
    ctx->executor_sleep_lock = 0;

    return executor;
}

/* TERMINATION HANDLERS */

template<typename Connection>
void stop_workers_and_exit(Executor<Connection>* executor) {
    Context* ctx = &executor->ctx;

    // if we're stopping outside of SIGTERM let it finish without interfering
    bool zero = false;
    bool is_first = atomic::cmpxchg(&ctx->is_stopping, &zero, 1);
    if (!is_first) {
        return;
    }

    // spin until each worker has finished
    lock::spinlock_wait(&ctx->workers_cleanup_lock, 0, 0);

    destroy(executor);
}

// used only by the signal handler, DON'T use it for anything else
__attribute__((__used__)) static void* global_executor = nullptr;

template<typename Connection>
void on_sigterm(int) {
    auto executor = reinterpret_cast<Executor<Connection>*>(global_executor);
    Context* ctx = &executor->ctx;

    stop_workers_and_exit(executor);
    global_executor = nullptr;

    // wake the main process and let it terminate gracefully
    lock::futex_wake(&ctx->executor_sleep_lock, 1);
}

/* STARTUP */

template<typename Callbacks, typename Connection>
ssize_t start(Executor<Connection>* executor) {
    Context* ctx = &executor->ctx;
    Config* config = &ctx->config;

    // setup the sigterm signal handler
    global_executor = executor;
    ssize_t ret = signal::sigaction_rt(SIGTERM, &on_sigterm<Connection>, 0, 0, nullptr);
    if (ret < 0) {
        errors::log_syscall(global_out, "sigaction_rt", ret);
        destroy(executor);
        return -1;
    }

    // create threads
    uint32_t thread_nr = ctx->config.thread_nr;
    size_t worker_size = ctx->worker_size;
    size_t stack_size = ctx->stack_size;
    auto stack_addr = reinterpret_cast<char*>(&executor->workers) + worker_size * thread_nr;

    // socket options
    struct sock_filter code[] = {

        // A = raw_smp_processor_id()
        {BPF_LD | BPF_W | BPF_ABS, 0, 0, 0U | SKF_AD_OFF + SKF_AD_CPU},

        // return A
        {BPF_RET | BPF_A, 0, 0, 0},
    };

    struct sock_fprog fprog = {
        .len = sizeof(code) / sizeof(*code),
        .filter = code,
    };

    int true_val = 1;
    struct {
        int opt;
        char* val;
        int opt_size;
    } opt[] = {
        {SO_REUSEADDR, reinterpret_cast<char*>(&true_val), sizeof(true_val)},
        {SO_REUSEPORT, reinterpret_cast<char*>(&true_val), sizeof(true_val)},
        {SO_ATTACH_REUSEPORT_CBPF, reinterpret_cast<char*>(&fprog), sizeof(fprog)},
    };

    // start the workers
    for (uint32_t i = 0; i < thread_nr; i++, stack_addr += stack_size) {
        Worker<Connection>* worker = reinterpret_cast<Worker<Connection>*>(reinterpret_cast<char*>(executor->workers) + worker_size * i);

        worker->ctx = ctx;
        worker->idx = i;
        connection::init_pool(&worker->conn_pool, config->conn_per_thread_nr);

        // increase the cleanup lock counter here
        // it's not set to thread_nr so that if the thread exits early
        // stop_workers_and_exit() waits for the correct number
        atomic::add(&ctx->workers_cleanup_lock, 1);

        // instance the socket and bind it
        // this must be done here so that the sockets are bound in order
        // and socket X is pinned on thread X to achieve perfect locality
        int socket_fd = static_cast<int>(socket(config->addr.v4.sin_family, SOCK_STREAM, IPPROTO_TCP));
        if (socket_fd < 0) {
            errors::log_syscall(global_out, "socket", socket_fd);

exit:
            // simulate the cleanup to keep the count correct
            atomic::add(&ctx->workers_cleanup_lock, -1);
            atomic::add(&ctx->workers_ready_lock, -1);

            stop_workers_and_exit(executor);
            return -1;
        }

        // remove the last option if the socket has already been bound
        size_t opt_len = sizeof(opt) / sizeof(*opt) - (i != 0);

        ssize_t ret;
        size_t j;
        for (j = 0; j < opt_len; j++) {
            ret = setsockopt(socket_fd, SOL_SOCKET, opt[j].opt, opt[j].val, opt[j].opt_size);
            if (ret < 0) {
                errors::log_syscall(global_out, "setsockopt", ret);

close_socket:
                ::close(socket_fd);
                goto exit;
            }
        }

        ret = ::bind(socket_fd, reinterpret_cast<sockaddr*>(&config->addr), config->addr_size);
        if (ret < 0) {
            errors::log_syscall(global_out, "bind", ret);
            goto close_socket;
        }

        // socket listen
        ret = listen(socket_fd, INT32_MAX);
        if (ret < 0) {
            errors::log_syscall(global_out, "listen", ret);
            goto close_socket;
        }

        worker->socket_fd = socket_fd;

        // start the thread
        worker->tid = process::spawn_thread(&worker::run<Callbacks, Connection>, worker, stack_addr, stack_size);
        if (worker->tid < 0) {
            errors::log_syscall(global_out, "clone3", ret);
            goto close_socket;
        }

        // pin the thread to a single core
        cpu_set_t cpu_set;
        process::cpu_set_idx(&cpu_set, i);
        ret = sched_setaffinity(worker->tid, sizeof(cpu_set_t), reinterpret_cast<unsigned long*>(&cpu_set));
        if (ret < 0) {
            errors::log_syscall(global_out, "sched_setaffinity", ret);
            stop_workers_and_exit(executor);
            return -1;
        }
    }

    // wait until every worker is ready
    lock::spinlock_wait(&ctx->workers_ready_lock, 0, 0);

    // if a worker errored just exit
    if (ctx->workers_cleanup_lock != ctx->config.thread_nr) {
        stop_workers_and_exit(executor);
        return -1;
    }

    // sleep until it's time to cleanup
    lock::futex_wait(&ctx->executor_sleep_lock, 0);

    return 0;
}

} // namespace faye::executor
