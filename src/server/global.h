#pragma once

#include <stddef.h>

#include <linux/time.h>

#include "../lib/io.h"
#include "../lib/lock.h"

namespace faye {

auto global_out = utter::output();

struct Config {
    // number of maximum conneciton handled per thread
    uint32_t conn_per_thread_nr;

    // number of thread (0 = all)
    uint32_t thread_nr;

    // *milliseconds* the kernel thread must wait before it starts sleeping
    unsigned loop_timeout_delay;

    // time that the server loop should wait before checking for events
    // if there isn't any event in the last iteration
    timespec loop_timeout_duration;

    int addr_size;

    // ip address and port
    union {
        sockaddr_in v4;
        sockaddr_in6 v6;
    } addr;
};

struct Context {
    // copy of the config struct passed by the user
    Config config;

    // true if the server is in the stopping phase (e.g. after sigterm)
    bool is_stopping;

    // true if the first worked has completed the setup phase
    bool is_first_worker_setup;

    // size in bytes of the worker
    size_t worker_size;

    // size in bytes of the thread stack
    size_t stack_size;

    // used to synchronize all the workers after the startup
    Lock workers_ready_lock;

    // used to synchronize all the workers during the cleanup phase
    Lock workers_cleanup_lock;

    // used exclusively as futex after the initialization phase
    // it's waken up before terminating after all the workers have terminated
    Lock executor_sleep_lock;
};

} // namespace faye
