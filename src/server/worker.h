#pragma once

#include "../lib/errors.h"
#include "../lib/io_uring.h"

#include "connection.h"
#include "global.h"

namespace faye {

template<typename Connection>
struct Worker {
    IoUring io_uring;
    Context* ctx;
    pid_t tid;
    int socket_fd;
    uint32_t idx;
    bool is_accepting;
    ConnectionPool<Connection> conn_pool;
};

constexpr uint32_t max_conn_idx = UINT32_MAX;

} // namespace faye

namespace faye::worker {

/* MAIN THREAD */

template<typename Connection>
constexpr size_t compute_size(unsigned conn_nr) {
    // clang-format off
    return sizeof(Worker<Connection>)
           + connection::compute_pool_size<Connection>(conn_nr)
           - sizeof(ConnectionPool<Connection>);
    // clang-format on
}

/* CLEANUP */

template<typename Connection>
void cleanup(Worker<Connection>* worker) {
    Context* ctx = worker->ctx;
    IoUring* io_uring = &worker->io_uring;

    ::close(worker->socket_fd);
    io_uring::clean(io_uring);
    atomic::add(&ctx->workers_cleanup_lock, -1);
}

template<typename Connection>
void slave_cleanup_and_exit(Worker<Connection>* worker) {
    // this is called when a thread terminates
    // - after another thread dies (master_cleanup_and_exit() gets called)
    // - after the main threads gets SIGTERM (on_sigterm() -> stop_workers_and_exit() gets called)

    cleanup(worker);
    exit(0);
}

template<typename Connection>
void master_cleanup_and_exit(Worker<Connection>* worker) {
    // this is called when a thread fails critically during execution
    // if more threads fail during the cleanup they just get rerouted to slave_cleanup_and_exit()

    Context* ctx = worker->ctx;

    // if we're already stopping (e.g. from SIGTERM, just do a slave cleanup
    bool zero = false;
    bool is_first = atomic::cmpxchg(&ctx->is_stopping, &zero, 1);
    if (!is_first) {
        return;
    }

    // else do like the sigterm handler does but we have to cleanup ourselves too!
    cleanup(worker);

    // wait for all the other threads
    lock::spinlock_wait(&ctx->workers_cleanup_lock, 0, 0);

    // wake up the main thread
    lock::futex_wake(&ctx->executor_sleep_lock, 1);

    exit(0);
}

/* INTERNAL API */

template<typename Connection>
FAYE_INLINE ssize_t accept(Worker<Connection>* worker) {
    IoUring* io_uring = &worker->io_uring;
    ConnectionPool<Connection>* conn_pool = &worker->conn_pool;

    uint32_t conn_idx = connection::borrow(conn_pool);
    Connection* conn = connection::get(conn_pool, conn_idx);
    BaseConnection* base_conn = reinterpret_cast<BaseConnection*>(conn);
    base_conn->is_open = false;

    io_uring_sqe* sqe = io_uring::get_sqe(io_uring, 0, 0, conn_idx);
    io_uring::accept(sqe, worker->socket_fd, reinterpret_cast<sockaddr*>(&base_conn->addr), &base_conn->addrlen, 0);
    io_uring::submit(io_uring);

    return io_uring::wakeup(io_uring, 1);
}

template<typename Connection>
ssize_t wait(Worker<Connection>* worker) {
    Context* ctx = worker->ctx;
    IoUring* io_uring = &worker->io_uring;

    io_uring_sqe* sqe = io_uring::get_sqe(io_uring, 0, 0, max_conn_idx);
    timeout(worker, sqe, &ctx->config.loop_timeout_duration, 0, 0);

    // block with getevents since there is no event to process
    unsigned flags = IORING_ENTER_GETEVENTS;
    if ((*io_uring->sq.flags & IORING_SQ_NEED_WAKEUP) != 0) {
        flags |= IORING_ENTER_SQ_WAKEUP;
    }

    return io_uring::enter(io_uring, 1, 1, flags, nullptr);
}

/* MAIN LOOP */

// user_data has low 32 bit -> conn idx, high 32 bit, io req idx (relative to the same conn)

template<typename Callbacks, typename Connection>
void run(Worker<Connection>* worker) {
    Context* ctx = worker->ctx;
    IoUring* io_uring = &worker->io_uring;
    ConnectionPool<Connection>* conn_pool = &worker->conn_pool;
    auto out = utter::output();

    // io_uring setup
    ssize_t ret = io_uring::init(io_uring, worker->idx, ctx->config.loop_timeout_delay);
    if (ret < 0) {
        errors::log_syscall(out, "io_uring_setup", ret);

clean_io_uring:
        io_uring::clean(io_uring);
        ::close(worker->socket_fd);

        atomic::add(&ctx->workers_cleanup_lock, -1);
        atomic::add(&ctx->workers_ready_lock, -1);
        exit(0);
    } else if (ret == 0) {
        utter::to(out, "io_uring_setup: mmap failed\n", utter::to_stderr);
        goto clean_io_uring;
    }

    // signal that the thread has finished the setup phase
    atomic::add(&ctx->workers_ready_lock, -1);

    // wait for all thread to have finished
    lock::spinlock_wait(&ctx->workers_ready_lock, 0, 0);

    // setup the first accept
    worker->is_accepting = true;
    accept(worker);

    while (FAYE_LIKELY(!ctx->is_stopping)) {
        // try and get and entry from the queue
        unsigned cqe_nr = io_uring::get_cqe_nr(io_uring);

        // wait for a completion if the queue is empty
        if (FAYE_UNLIKELY(cqe_nr == 0)) {
            ret = wait(worker);

            // if we fail here there's something really serious going on
            if (FAYE_UNLIKELY(ret < 0)) {
                errors::log_syscall(out, "io_uring_enter", ret);
                master_cleanup_and_exit(worker);
            }

            // now we're sure there's at least one entry to process (the timeout)
            cqe_nr = io_uring::get_cqe_nr(io_uring);
        }

        do {
            io_uring_cqe* cqe = io_uring::get_cqe(io_uring);
            uint32_t conn_idx = cqe->user_data;

            // no connection associated --> nothing to do
            if (conn_idx == max_conn_idx) {
                io_uring::mark_cqe_complete(io_uring);
                cqe_nr--;
                continue;
            }

            Connection* conn = connection::get(conn_pool, conn_idx);
            auto base_conn = reinterpret_cast<BaseConnection*>(conn);

            if (!base_conn->is_open) {
                // set the state as open
                base_conn->is_open = true;
                base_conn->fd = cqe->res;

                if (FAYE_LIKELY(!connection::is_pool_empty(conn_pool))) {
                    // after an accept queue another accept if possible
                    accept(worker);
                } else {
                    // set the flag so the accept gets queued during the next close_conn
                    worker->is_accepting = false;

                    // notify the user
                    Callbacks::on_empty_pool(worker);
                }
            } else {
                // after any other syscall just set the result
                base_conn->last_ret = cqe->res;
            }

            // mark the cqe as complete
            io_uring::mark_cqe_complete(io_uring);
            cqe_nr--;

            // handle the event
            Callbacks::on_request(worker, cqe->user_data);
        } while (cqe_nr != 0);
    }

    slave_cleanup_and_exit(worker);
}

} // namespace faye::worker
